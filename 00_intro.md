# Introduction

This tutorial series aim to do two things. First, it's a guide on how to build a 2D game engine with
Go and OpenGL. It covers low-level concepts like drawing sprites using shaders and high-level
concepts like game object hierarchies and component systems. Common game algorithms like collision
detection, pathfinding, procedural generation and AI are also explored.

Second, this guide is meant to document the [AGL game engine](https://gitlab.com/onikolas/agl). AGL
is hobby 2D game engine built with Go and OpenGL. AGL started as an attempt to write a GPU
accelerated text editor and slowly evolved into a sprite renderer and finally into a game engine
(yay for feature creep!). Most of this tutorial series is based on the work done for AGL and is in a
sense a step-by-step recreation of this engine.

Having a working game engine as the basis of this tutorial series gives the user the advantage of
being able to skip the parts of the tutorial that they don't want to do. Not interested in OpenGL
programming? Matrix multiplication too boring? No problem, you can skip ahead to the game loop and
collisions. In throwaway tutorial code this could leave you with gaps that could later come back to
bite you but AGL provides proper abstractions (tries to anyway:). You can render sprites without
ever having to worry about OpenGL or shaders. This way, parts that you skip are "black boxes"
that you only need to open if  you are ever curious about how they work.

Having said that, using AGL is not a requirement and if you want to build an engine from scratch you
will be able to. To enable this, the first few tutorials in the series are accompanied with standalone
examples as a way to get started without referencing code from a larger code base.

## Why Build a Game Engine

Why build a game engine? Lets first say that if your goal is to make a game you don't need to build
a game engine. Just use one of the many, excellent, game engines available out there. Unreal Engine
and Unity are the more popular ones, and Godot is an excellent open source option[^1].  It might
feel like overkill to be using something as powerful and feature-rich as Unreal Engine or Unity for
a simple 2D game but the time spent learning these tools is well worth it. It will be significantly
more time consuming to implement sprite drawing from scratch than learning how to draw sprites in
Unity, for example. 

With that out of the way, building a game engine is a rewarding experience and an excellent learning
opportunity. Engine programming deals with low-level concepts like memory management, optimization,
and graphics programming but also with high-level concepts such as program structure, abstractions
and even user-experience (if you are building a UI level editor for example). 

## Why 2D

Firstly, it was what I needed for my own project which was originally to write a GPU accelerated
text editor. Secondly, it's easier than 3D.  In 3D we are displaying  3D models made of many
triangles. To animate a model we need a control rig (or skeleton) and some way to map the rig to
the triangles of the model. We then need animation data to move the rig, which in turn moves the
model. On top of all that we need textures and materials to make the model look good. In 2D we are
just displaying images. Once we can show one image on screen we are done, animation is just showing
more images in succession.

It is also easier to produce content for a 2D game. Everyone can draw but not everyone can do 3D
modeling and animation. This is not to say that every programmer is an artist or that 2D is easy to
do. To make a good 2D game we need good 2D art and that's something only a skilled 2D artist can
produce. However, unlike 3D, with 2D we can at least draw some test art and get the process going.

It should be said that while 2D is easier, it does expose the user to many of the same concepts as
3D. We will still have to deal with triangles, transformations, matrices and  calls to send
data to the GPU. We will still be exposed to shader coding. Also, many of the algorithms in game
programming are the same or similar for 2D and 3D. Scene partitioning is done in both 2D and 3D
games but in 3D there is an extra dimension. Pathfinding is usually done in 2D even in 3D games. The
game loop is timed in the same way in both. The principles of collision detection are the same.
This is all to say that you will not be cheating yourself by learning game programming in 2D. In
fact we believe that 2D is the best way to start learning game programming as it exposes you to all
the important concepts with as little complication as possible.

## Why Go

Go is a good compromise between the performance of low level languages like C/C++ and the
easy-of-use and productivity of high-level languages like Python. It is also what the author likes
to write:). Other engines are implemented in C or C++ but the user codes in higher-level
languages. For example Godot users code in the Python-esque GDScript. In this series, and in AGL,
the intention is that the user programs in Go and the engine is simply a dependency that the user
includes[^2]. 

You don't need to be a Go programmer to go through these tutorials. Go is very easy to pickup and we
will make an attempt to explain language-specific features as they come up. If you want a more
structured way to learn Go, the [tour](https://go.dev/tour) is an excellent starting point.

[^1]: There are many, many more https://en.wikipedia.org/wiki/List_of_game_engines.
[^2]: In Go, libraries are compiled from source and is easy to view the source code of any library
    that you include. We believe this is a great learning tool and one of the best features of the
    language.

## Demo Game

As part of this tutorial series we build a simple hack and slash game called Knight vs Trolls. 

![](img/23_levels.webm){width=1000px}

Although simple, it showcases many aspects of what make a fully-fledged game.

## Release Plan 

The aim is to have a new tutorial released every other week. Use this [RSS feed](feed) to get
updates. 


## Setup

Before we jump to the first tutorial we need to do a bit of setup. The first step is to instal
Go. You can get it from the [Go website](https://go.dev/) or instal from the package manager of your
distribution. Go is likely supported in your IDE but you might have to add a plugin, see the
[wiki](https://github.com/golang/go/wiki/IDEsAndTextEditorPlugins) for more info. Once you have Go
installed go through [these](https://go.dev/doc/tutorial/getting-started) steps that will show you
how to create, build and run a hello world Go program. With that done, you are ready to start
the tutorial.


## Source

The source code for all examples in this series can be found in this repo:

[https://gitlab.com/onikolas/agl-docs-src](https://gitlab.com/onikolas/agl-docs-src)
