import matplotlib.pyplot as plt

def set(figsize=(5,5)):
    textColor = 'white'
    plt.rcParams['text.color'] = textColor
    plt.rcParams['axes.labelcolor'] = textColor
    plt.rcParams['xtick.color'] = textColor
    plt.rcParams['ytick.color'] = textColor
    plt.rcParams['figure.facecolor']=  (1.0, 1.0, 1.0, 0.0)  
    plt.rcParams['savefig.facecolor']= (0.0, 0.0, 0.0, 0.0)
    plt.rcParams['axes.facecolor']=  (0.0, 0.0, 0.0, 0.0)
    plt.rcParams['lines.color'] = '#88C0D0'
    plt.rcParams['figure.figsize'] = figsize 
    ax = plt.gca()                         
    ax.spines['bottom'].set_color('white') 
    ax.spines['top'].set_color('white')    
    ax.spines['left'].set_color('white')   
    ax.spines['right'].set_color('white')  

