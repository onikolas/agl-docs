import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Polygon

import style
style.set()
fns = 10

pts = np.array([
    [-3,  -4],
    [-1, -4],
    [-1, -1],
    [-3, -1]
])
p1 = Polygon(pts, closed=True, fc='None', ec='white')
plt.text(-3.5,-4.5, "A.P1", fontsize=fns, color='white')
plt.text(-1,-1, "A.P2", fontsize=fns, color='white')

pts = np.array([
    [-2, -2],
    [2, -2],
    [2, 1],
    [-2, 1]
])
p4 = Polygon(pts, closed=True,  fc='None', ec='white')
plt.text(-2.5,-2.5, "B.P1", fontsize=fns, color='white')
plt.text(2,1, "B.P2", fontsize=fns, color='white')


 
ax = plt.gca()
ax.add_patch(p4)
ax.add_patch(p1)
ax.set_xlim(-5,3)
ax.set_ylim(-5,3)
ax.axis('off')


plt.savefig("17_overlap.png")

