import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Polygon

import style
style.set()

pts = np.array([
    [-0.5, -0.5],
    [0.5, -0.5],
    [-0.5, 0.5],
])
p1 = Polygon(pts, closed=True, fc='grey', ec='white')

pts = np.array([
    [-0.5, 0.5],
    [0.5, -0.5],
    [0.5, 0.5]
])
p2 = Polygon(pts, closed=True, fc='grey', ec='white')

ax = plt.gca()
ax.add_patch(p1)
ax.add_patch(p2)
ax.set_xlim(-1,1)
ax.set_ylim(-1,1)
#ax.axis('off')

fns = 10
plt.text(-0.7,-0.6, "-0.5,-0.5", fontsize=fns, color='white')
plt.text(0.5,0.52, "0.5,0.5", fontsize=fns, color='white')
plt.text(-0.7,0.52, "-0.5,0.5", fontsize=fns, color='white')
plt.text(0.5,-0.6, "0.5,-0.5", fontsize=fns, color='white')


# Move left y-axis and bottom x-axis to centre, passing through (0,0)
ax.spines['left'].set_position('center')
ax.spines['bottom'].set_position('center') 
 
# Eliminate upper and right axes
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')

# Show ticks in the left and lower axes only
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')

plt.xticks(np.arange(-1, 2, 1))
plt.yticks(np.arange(-1, 2, 1))


plt.savefig("07_template.png") 
 
