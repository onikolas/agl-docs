import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Polygon

import style
style.set()

pts = np.array([
    [-0.5, -0.5],
    [0.5, -0.5],
    [0.5, 0.5],
    [-0.5, 0.5]
])
p1 = Polygon(pts, closed=True, fc='grey', ec='white')


translation = np.array([1.5, -1])

pts = np.array([
    [-0.5, -0.5],
    [0.5, -0.5],
    [0.5, 0.5],
    [-0.5, 0.5]
]) +translation
p4 = Polygon(pts, closed=True,  fc='grey', ec='white')

 
ax = plt.gca()
ax.add_patch(p1)
ax.add_patch(p4)
ax.set_xlim(-2,2.01)
ax.set_ylim(-2,2.01)
#ax.axis('off')

# Move left y-axis and bottom x-axis to centre, passing through (0,0)
ax.spines['left'].set_position('center')
ax.spines['bottom'].set_position('center')

# Eliminate upper and right axes
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')

# Show ticks in the left and lower axes only
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')

plt.arrow(0.0,0.0, translation[0], translation[1],head_width=0.05, color='#88C0D0')

plt.xticks(np.arange(-2, 3, 1))
plt.yticks(np.arange(-2, 3, 1))
ax.spines['left'].set_position('zero')
ax.spines['bottom'].set_position('zero')


plt.savefig("07_translation.png")

