import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Polygon, Circle


import style
style.set(figsize=(3.07*2,1.07*2))

pts1 = np.array([[0,0], [0,1], [3,1], [3,0]])
pts2 = np.array([[1,0], [1,1], [2,1], [2,0]])

p1 = Polygon(pts1, closed=True, fc='grey', ec='white')
p2 = Polygon(pts2, closed=True, fc='grey', ec='white')
c1 = Circle((1,0), radius=0.02, color='#88C0D0')
c2 = Circle((2,1), radius=0.02, color='#88C0D0')

fns = 10 
plt.text(0.8, -0.2, "(0.33,1)", fontsize=fns, color='white') 
plt.text(1.8, 1.03, "(0.66,0)",  fontsize=fns, color='white') 
ax = plt.gca()
ax.add_patch(p1)
ax.add_patch(p2)
ax.add_patch(c1)
ax.add_patch(c2)
ax.set_xlim(-0.2,3.05)
ax.set_ylim(-0.2,1.05)
ax.axis('off')


plt.savefig('08_bounding_box.png')   
 
  
