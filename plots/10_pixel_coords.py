import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Polygon
import math

import style
style.set(figsize=(3,3))

t = math.pi/4
pts = np.array([
    [1, 1],
    [3, 1],
    [3, 3],
    [1, 3]
]) 


p1 = Polygon(pts, closed=True, fc='grey', ec='white')


ax = plt.gca()
ax.add_patch(p1)
ax.set_xlim(0,5)
ax.set_ylim(0,5)
#ax.axis('off')

# Move left y-axis and bottom x-axis to centre, passing through (0,0)
ax.spines['left'].set_position('center')
ax.spines['bottom'].set_position('center')

# Eliminate upper and right axes
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')

# Show ticks in the left and lower axes only
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')

plt.xticks(np.arange(0, 6, 1))
plt.yticks(np.arange(0, 6, 1))
ax.spines['left'].set_position('zero')
ax.spines['bottom'].set_position('zero')


plt.savefig("10_pixel_coords.png")


