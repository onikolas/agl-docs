import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Polygon

import style
style.set(figsize=(3,3))

pts = np.array([[0,0], [0,1], [1,0]])


p = Polygon(pts, closed=True, fc='grey', ec='white')
fns = 10
plt.text(-0.1,-0.1, "(0,0,0)", fontsize=fns, color='white')
plt.text(0.98,-0.1, "(1,0,0)",  fontsize=fns, color='white')
plt.text(0,1.02, "(0,1,0)",  fontsize=fns, color='white')
ax = plt.gca()
ax.add_patch(p)
ax.set_xlim(-0.1,1.1)
ax.set_ylim(-0.1,1.1)
ax.axis('off')
plt.savefig('01_triangle.png')   
 
  
