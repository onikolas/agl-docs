import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Polygon
import math

import style
style.set()

def rotate(points, t):
    pts = []
    for p in points:
        x = p[0]
        y = p[1]
        nx = x*math.cos(t)-y*math.sin(t)
        ny = x*math.sin(t)+y*math.cos(t)
        pts.append([nx, ny])
    return np.array(pts)        
        
 
t = math.pi/6
pts = np.array([
    [-0.5, -0.5],
    [0.5, -0.5],
    [0.5, 0.5],
    [-0.5, 0.5]
])

rotated_points = rotate(pts, t)

p1 = Polygon(rotated_points, closed=True, fc='grey', ec='white')




ax = plt.gca()
ax.add_patch(p1)
ax.set_xlim(-1,1)
ax.set_ylim(-1,1)
#ax.axis('off')

# Move left y-axis and bottom x-axis to centre, passing through (0,0)
ax.spines['left'].set_position('center')
ax.spines['bottom'].set_position('center')

# Eliminate upper and right axes
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')

# Show ticks in the left and lower axes only
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')

plt.xticks(np.arange(-1, 2, 1))
plt.yticks(np.arange(-1, 2, 1))

plt.savefig("07_rotate.png")


