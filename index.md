# AGL

AGL is a 2D renderer and game engine written in Go. It uses OpenGL to render. It can do the following:

- Render sprites from a spritesheet (sprite atlas)
- Automatically manage images into spritesheets
- Text rendering (creates spitesheets from truetype fonts)
- Lets you write GLSL shaders to be used with sprites (default shaders) 
- Input (through SDL)
- Collisions (hitbox type, no physics yet)
- Pathfinding 
- GameObjects and Components 
- Simple scene graph
- Basic UI 

Link to the [code](https://gitlab.com/onikolas/agl) and [docs](https://pkg.go.dev/gitlab.com/onikolas/agl).

I am writting a tutorial series for AGL, and game engines in general. It's [here](00_intro.html).



