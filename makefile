TARGET = index.html 00_intro.html 01_rendering_a_sprite.html 02_animating.html 03_game_loop.html \
	04_rendering_arch.html 05_sprite_atlas.html 06_shader_type.html 07_default_shader.html \
	08_instancing.html 09_buffers.html 10_sprite.html 11_renderer.html 12_renderer_demo.html \
	13_game_object.html 14_go_hierarchy.html 15_animation_component.html 16_input.html \
	17_collisions.html 18_game_object2.html 19_drawing_text.html 20_audio.html 21_kvst_part1.html \
	22_kvst_part2.html 23_kvst_part3.html\

NIKOS_DIR = /home/nik/nikolas/do/nik-os.com/agl

all: $(TARGET)
	$(MAKE) -C plots

index.html: index.md template-main.html
	pandoc -i  $< -o $@ --template=template-main.html

%.html: %.md template.html feed codedoc.css
	pandoc -i --mathml $< -o $@ --template=template.html

clean:
	rm -f $(TARGET) 
	rm -f plots/*png

toDeploy:  all
	cp $(TARGET) $(NIKOS_DIR)
	rsync --delete -aP img/ $(NIKOS_DIR)/img/
	cp codedoc.css $(NIKOS_DIR)
	cp feed $(NIKOS_DIR)

